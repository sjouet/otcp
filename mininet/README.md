Basic Mininet startup script to instantiate a simple three layer topology with
an increasing latency at each layer

  * core to agg latency is 1ms
  * agg to tor latency is 0.2ms

By default all the links are configured to be gigabit and all the switches
have a maximum queue size of 60 packets

  * ECN can be enabled by passing the flag --ecn to the program
  * HFSC can be enable for the switches using --use-hfsc

To enable DCTCP you need a kernel > 3.18 and enable DCTCP as the congestion
control to be used.
