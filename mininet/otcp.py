#!/usr/bin/python
# -*- coding: utf-8 -*-

CONTROLLER_PATH = '/media/sf_NOMS2016/controller/controller'

import subprocess
import re
import time
import argparse

from functools import partial
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg
from mininet.node import CPULimitedHost, RemoteController, OVSSwitch
from mininet.link import TCLink
from mininet.util import irange, custom, quietRun, dumpNetConnections
from mininet.cli import CLI

parser = argparse.ArgumentParser(description="OTCP Experimental Setup")

parser.add_argument('--tcpdump',
                    dest='tcpdump',
                    action='store_true',
                    help='Measure latency using TCPdump instead of OTCP',
                    default=False)

parser.add_argument('--ecn',
                    dest='ecn',
                    action='store_true',
                    help='Enable ECN (net.ipv4.tcp_ecn)',
                    default=False)

parser.add_argument('--use-hfsc',
                    dest="use_hfsc",
                    action="store_true",
                    help="Use HFSC qdisc",
                    default=True)

args = parser.parse_args()

def enable_tcp_ecn():
    subprocess.Popen("sysctl -w net.ipv4.tcp_ecn=1", shell=True).wait()

def disable_tcp_ecn():
    subprocess.Popen("sysctl -w net.ipv4.tcp_ecn=0", shell=True).wait()

class ExperimentTopo(Topo):
    def __init__(self):
        # Add default members to class
        super(ExperimentTopo, self).__init__()

        tor_lconfig  = {
            'bw':    1000,
            # 'delay': '0.1ms'
        }
        agg_lconfig  = {
            'bw':    1000,
            'delay': '0.2ms'
        }
        core_lconfig = {
            'bw':    1000,
            'delay': '1ms'
        }
        switch_config = {
            'enable_ecn': args.ecn,
            'use_hfsc': args.use_hfsc,
            'max_queue_size': 60
        }

        # Just create a partial topology for the experiment to save resources
        self.addSwitch('core', dpid='0000000000000001', **switch_config)
        self.addSwitch('agg1', dpid='0000000000000002', **switch_config)
        self.addSwitch('agg2', dpid='0000000000000003', **switch_config)
        self.addSwitch('tor1', dpid='0000000000000004', **switch_config)
        self.addSwitch('tor2', dpid='0000000000000005', **switch_config)
        self.addSwitch('tor3', dpid='0000000000000006', **switch_config)

        # Connect the switches together
        self.addLink('core', 'agg1', **core_lconfig)
        self.addLink('core', 'agg2', **core_lconfig)
        self.addLink('agg1', 'tor1', **agg_lconfig)
        self.addLink('agg1', 'tor2', **agg_lconfig)
        self.addLink('agg2', 'tor3', **agg_lconfig)

        # the 10 hosts under the same ToR
        host_format = 'h{:02x}{:02x}{:02x}'
        for i in range(10):
            hostname = host_format.format(1, 1, i+1)
            self.addHost(hostname, ip='10.1.1.{}'.format(i+1), mac='00:00:00:01:01:{:02X}'.format(i+1)) # AggId.TorId.HostId
            self.addLink('tor1', hostname, **tor_lconfig)

        # one host under same agg different tor
        hostname = host_format.format(1, 2, 1)
        self.addHost(hostname, ip='10.1.2.1', mac='00:00:00:01:02:01')
        self.addLink('tor2', hostname, **tor_lconfig)

        # one host under different agg
        hostname = host_format.format(2, 1, 1)
        self.addHost(hostname, ip='10.2.1.1', mac='00:00:00:02:01:01')
        self.addLink('tor3', hostname, **tor_lconfig)

def main():
    # Start the controller
    #controller_proc = subprocess.Popen([CONTROLLER_PATH], stderr=subprocess.PIPE)
    #time.sleep(3)

    # Create the mininet network
    controller = RemoteController('controller')
    topo = ExperimentTopo()
    switch = partial(OVSSwitch, protocols='OpenFlow13')
    net = Mininet(topo=topo, link=TCLink, autoSetMacs=True, controller=controller, switch=switch)

    # Start the network
    net.start()

    for switchName in topo.switches():
        # Insert a rule to redirect LLDP packets to the controller
        subprocess.Popen('ovs-ofctl add-flow {} priority=1,dl_dst=01:80:c2:00:00:0e,action=CONTROLLER -O OpenFlow13'.format(switchName), shell=True).wait()
        # Insert default learning switch
        subprocess.Popen('ovs-ofctl add-flow {} priority=0,action=NORMAL -O OpenFlow13'.format(switchName), shell=True).wait()

        # Start capturing the traffic on the switchs' interfaces
        if args.tcpdump:
            interfaces = {
                'core': ['eth1', 'eth2'],
                'agg1': ['eth1', 'eth2', 'eth3'],
                'agg2': ['eth1', 'eth2'],
                'tor1': ['eth1'],
                'tor2': ['eth1'],
                'tor3': ['eth1'],
            }

            for k,v in interfaces.items():
                node = net.getNodeByName(k)
                for ifname in v:
                    fname = '{}-{}'.format(k, ifname)
                    node.popen('tcpdump -i {0} -w pcaps/{0}.pcap ether src 00:00:00:00:00:01 and ether dst 01:80:c2:00:00:0e '.format(fname), shell=True)

    # trigger a pingAllFull to solve the ARP and the delay associated
    net.pingAllFull()

    CLI(net)

    # Stop mininet
    net.stop()

    # Kill the controller process
    controller_proc.terminate();

    # restore TCP parameters
    disable_tcp_ecn()

    # Get the output from the controller
    controller_stdout, controller_stderr = controller_proc.communicate()
    print controller_stdout

if __name__ == '__main__':
    main()
