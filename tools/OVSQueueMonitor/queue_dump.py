import time
import signal
import sys
import cPickle as pickle
from pyroute2 import IPRoute
from pyroute2 import IPDB

ipdb = IPDB()
ip = IPRoute()

iface_name = 'p1p1'

records = []

iface = ipdb.interfaces.get(iface_name, None)

def signal_handler(signal, frame):
    with open('records.dump', 'wb') as f:
        r = []
        for record in records:
            for a in record[1][0]['attrs']:
                if a[0] == 'TCA_STATS':
                    r.append((record[0], a[1].items()))

        pickle.dump(r, f)
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

while True:
    r = ip.get_qdiscs(iface['index'])
    records.append((time.clock(), r))

