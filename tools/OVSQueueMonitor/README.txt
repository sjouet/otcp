Simple tool that polls the NIC as fast as possible using IPRoute to get queue
occupancy information.

PyPy performs significantly faster than python for this piece of code (around 2x).
