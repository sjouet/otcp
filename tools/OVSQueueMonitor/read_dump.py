import cPickle as pickle
import matplotlib.pyplot as plt

"""(16.406927, [('pps', 0), ('qlen', 0), ('bytes', 752183392L), ('drop', 0), ('packets', 496859), ('overlimits', 595545), ('bps', 0), ('backlog', 0)])"""

x_points = []
y_points = []

with open('records.dump','rb') as f:
    records = pickle.load(f)
    for t, stats in records:
        x_points.append(t)
        for r in stats:
            if r[0] == 'qlen':
                y_points.append(r[1])
                break

            if r[0] == 'drop':
                print 'got a drop !!!'
                break

# Some dirty filtering at the head
while y_points[:2] == [0, 0]:
   x_points = x_points[1:]
   y_points = y_points[1:]

# Some dirty filtering at the tail
while y_points[-2:] == [0, 0]:
    x_points = x_points[:-1]
    y_points = y_points[:-1]

# Apply the time bias
x_points = map(lambda t: t-x_points[0], x_points)

plt.plot(x_points, y_points)
plt.savefig('plot.png')
