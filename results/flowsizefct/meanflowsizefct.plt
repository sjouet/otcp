set terminal postscript eps color enhanced "Helvetica" 20 size 5,2
set output "meanflowsizefct.eps"

set size ratio 0.3

set xlabel "Flow Size (packets)"
set ylabel "Flow Completion Time (ms)"


set auto x
set yrange [0.001:2]
set logscale y

set style func linespoints
set key left

plot 'tcp.dat'   using 1:5 with linespoint pt 6 ps 2 lw 2 title 'TCP'      ,\
     'dctcp.dat' using 1:5 with linespoint pt 2 ps 2 lw 2 title 'DCTCP'    ,\
     'otcp.dat'  using 1:5 with linespoint pt 4 ps 2 lw 2 title 'OTCP'     ,\
     'odctcp.dat'  using 1:5 with linespoint pt 8 ps 2 lw 2 title 'ODCTCP'
exit
