import numpy as np
import matplotlib.pyplot as plt

for layer in ['tor', 'agg', 'core']:
    for alg in ['dctcp', 'otcp']:
        filename = '{}-{}.dat'.format(layer, alg)
        with open(filename) as f:
            points = [ float(line)*1000 for line in f.readlines() ]

            hist, bin_edges = np.histogram(points, range=(0, 1200), bins=120, density=True)

            cdf = [ sum(hist[:i+1])*10 for i in range(len(hist)) ]

            print filename
            print zip(bin_edges, cdf)
            #with open('cdf-{}-{}.dat'.format(layer, alg), 'w') as out:
                #for x,y in zip(bin_edges, cdf):
                    #out.write('{}\t{}\n'.format(x, y))
