set terminal pdfcairo font "Helvetica,9" fontscale 1.0
set output "flowlayer.pdf"

set auto x

set yrange [ 0 : 1.02 ]
set xrange [ 0 : 1100 ]

set style func linespoints
set key bottom right

set xlabel "Flow Completion Time (ms)"
set ylabel "Cumulative Distribution Function"

plot 'cdf-tor-dctcp.dat'    using 1:2 pointinterval 5 with linespoint title 'TOR DCTCP' ,\
     'cdf-agg-dctcp.dat'  using 1:2 pointinterval 5 with linespoint title 'Agg DCTCP' ,\
     'cdf-core-dctcp.dat'   using 1:2 pointinterval 5 with linespoint title 'Core DCTCP' ,\
     'cdf-tor-otcp.dat' using 1:2 pointinterval 5 with linespoint title 'TOR OTCP' ,\
     'cdf-agg-otcp.dat' using 1:2 pointinterval 5 with linespoint title 'Agg OTCP' ,\
     'cdf-core-otcp.dat' using 1:2 pointinterval 5 with linespoint title 'Core OTCP'
exit
