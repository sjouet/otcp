set terminal pdfcairo font "Helvetica,9" fontscale 1.0 size 5,2
set output 'latency.pdf'

set auto x
set yrange [0:1100]
set style data histogram
set style histogram errorbars
set style fill pattern border -1
set boxwidth 0.9
set xlabel "Link Layer"
set ylabel "Measured one way\nlatency({/Symbol m}s)"
set key top left
plot 'latency.dat' using 2:3:xtic(1) ti col, '' u 4:5 ti col, '' u 6:7 ti col
