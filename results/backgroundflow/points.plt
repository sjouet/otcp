set terminal pdfcairo font "Helvetica,9" fontscale 1.0 size 5,1.8
set output 'backgroundflow.pdf'

set auto x
set yrange [0:2.6]

set style data histograms
set style fill pattern 1

set xlabel "Congestion Control Algorithm"
set ylabel "Mean FCT (s)"

set key off
plot 'points.dat' using 2:xtic(1) ti col
